# -*- mode: python; coding: utf-8 -*-
# Re2o est un logiciel d'administration développé initiallement au rezometz. Il
# se veut agnostique au réseau considéré, de manière à être installable en
# quelques clics.
#
# Copyright © 2017  Gabriel Détraz
# Copyright © 2017  Goulven Kermarec
# Copyright © 2017  Augustin Lemesle
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


### Specify each interface role

interfaces_type = {
    'routable' : ['eth1', 'eth2'],
    'sortie' : ['eth3', 'eth4'],
    'admin' : ['eth5', 'eth6']
}

### Specify nat settings: name, interfaces with range, and global range for nat
### WARNING : "interface_ip_to_nat' MUST contain /24 ranges, and ip_sources MUST
### contain /16 range

nat = [
    {
        'name' : 'nat1',
        'interfaces_ip_to_nat' : {
            'eth1' : '185.230.76.0/24',
            'eth2' : '138.230.76.0/24',
        },
        'ip_sources' : '10.42.0.0/16'
    },
    {
        'name' : 'nat2',
        'interfaces_ip_to_nat' : {
            'eth1' : '185.230.77.0/24',
            'eth3' : '138.1.145.0/24'
        },
        'ip_sources' : '10.43.0.0/16'
    }
]
